FROM golang
WORKDIR /usr/src/app
COPY . .
RUN go build src/service.go
EXPOSE 8080
CMD ["./service"]


